document.getElementById("hello_text").textContent = "はじめてのJavaScript";
/* 文字を変化させている(代入ではない？) */

/* varは数字も文字も配列も関数もなんでもいけそうな変数型？
　 関数内でvar定義するとローカル変数になる */
var count = 0;
var cells;

/* ブロックのパターン定義 */
var blocks = { // 呼び出すときは blocks["クラス名"] 添え字はString型なので注意
  i:{
    class: "i",
    pattern:[
      [1,1,1,1]
    ]
  },
  o:{
    class: "o",
    pattern:[
      [1,1],
      [1,1]
    ]
  },
  s:{
    class: "t",
    pattern:[
      [0,1,0],
      [1,1,1]
    ]
  },
  s:{
    class: "s",
    pattern:[
      [0,1,1],
      [1,1,0]
    ]
  },
  z:{
    class: "z",
    pattern:[
      [1,1,0],
      [0,1,1]
    ]
  },
  j:{
    class: "j",
    pattern:[
      [1,0,0],
      [1,1,1]
    ]
  },
  l:{
    class: "l",
    pattern:[
      [0,0,1],
      [1,1,1]
    ]
  }
};

loadTable();
/* setInterval(実行する関数, 繰り返す間隔(ms)
　 下の場合だとfunction()を1s間隔で実行 */
setInterval(function(){
    count++;
    document.getElementById("hello_text").textContent = "はじめてのJavaScript(" + count + ")"; //文字列の連結はjavaと同じ
    if(hasFallingBlock()){
      fallBlocks();
    }else{
      deleteRow();
      generateBlock();
    }
},1000);

/* 以下関数定義 */

function loadTable(){ // テーブルをロードする
  var td_array = document.getElementsByTagName("td"); //200個の要素を持つ配列
  cells = []; //グローバル変数
  var index = 0;
  /* 配列cellsのrow行目のcol列目に、td_arrayのindex番目を代入*/
  for(var row = 0; row < 20; row++){
    cells[row] = [];
    for(var col = 0; col < 10; col++){
      cells[row][col] = td_array[index];
      index++;
    }
  }
}

function fallBlocks(){ // 落ちてくるブロックの処理をする
  /* 底についていないか？ */
  for(var col = 0; col < 10; col++){
    if(cells[19][col].blockNum === fallingBlockNum){ // 落ちてるブロックが一番下なら
      isFalling = false;
      return ;
    }
  }

  /* 落ちてるブロックの1マス下にブロックはないか？ */
  for(var row = 18; row >= 0; row--){
    for(var col = 0; col < 10; col++){
      if(cells[row][col].blockNum === fallingBlockNum){ // (row,col)が今落ちてるブロックなら
        if(cells[row + 1][col].className !== "" && cells[row + 1][col].blockNum !== fallingBlockNum){ // 落ちてるブロックの真下が空白以外かつ自分のパーツ以外なら
          isFalling = false;
          return ;
        }
      }
    }
  }

  /* 下から2行目(18行目)から順にクラスを1段下げる */
  for(var row = 18; row >= 0; row--){
    for(var col = 0; col < 10; col++){
      if(cells[row][col].blockNum === fallingBlockNum){ //もし(row,col)が落ちている
        cells[row + 1][col].className = cells[row][col].className; //(row,col)の一つ下の行にそれを代入し
        cells[row + 1][col].blockNum = cells[row][col].blockNum;
        cells[row][col].className = ""; //(row,col)を空にする
        cells[row][col].blockNum = null;
      }
    }
  }
}

var isFalling = false;
function hasFallingBlock(){ // 落下中のブロックがあるか確認する
  return isFalling;
}

function deleteRow(){ // そろっている行を全て消す

}

var fallingBlockNum = 0;
function generateBlock(){ // ランダムにブロックを生成する
  /* ランダムにブロックパターンを選ぶ */
  var keys = Object.keys(blocks); // 名前(string)の配列を取得 添え字は当然数字
  var nextBlockKey = keys[Math.floor(Math.random() * keys.length)]; // 名前から要素(string)を取得
  var nextBlock = blocks[nextBlockKey];
  var nextFallingBlockNum = fallingBlockNum + 1;
  /* ブロックを配置する */
  var pattern = nextBlock.pattern;
  for(var row = 0; row < pattern.length; row++){
    for(var col = 0; col < pattern[row].length; col++){
      if(pattern[row][col]){
        cells[row][col + 3].className = nextBlock.class;
        cells[row][col + 3].blockNum = nextFallingBlockNum;
      }
    }
  }
  /* 落下中のブロックがあるとする */
  isFalling = true;
  fallingBlockNum = nextFallingBlockNum;
}

function moveRight(){ // ブロックを右に移動させる

}

function moveLeft(){ // ブロックを左に移動させる

}
